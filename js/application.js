var brand = new brandController();

var user = new UserController();

var interactions = new InteractionsController();
//
listarTodos();

brand.listBrands(function(res){
    var htmlbrand = '';


    for(brand in res){
        var shares = res[brand].interactions.shares;
        var comments = res[brand].interactions.comments;
        var favorites = res[brand].interactions.favorites;
        var image = res[brand].image;
        htmlbrand += '<li class="brand-item" data-id="'+res[brand].id+'" data-name="'+res[brand].name+'"><div class="brand-image"><img src="'+image+'" alt="" /></div><div class="interactions"><ul><li><img src="imagens/share.svg" alt="shares"/> <span>'+shares+'</span></li><li><img src="imagens/chat.svg" alt="Comments"/> <span>'+comments+'</span></li><li><img src="imagens/star.svg" alt="Favorites"/> <span>'+favorites+'</span></li></ul></div></li>'
    }

    $('.brand-list').append(htmlbrand);
});

$('body').on('click', '.brand-item',function(){
    var brand = new brandController();
    var brandId = $(this).attr('data-id');
    var brandName = $(this).attr('data-name');

    brand.filterBrandUsers(brandId, function(res){
        makeListUsers(res);
        setFilterItens(brandName);
    })
});


$('body').on('click', '.details', function(){
    $target = $(this).attr('data-id');

    $('#card__canvas'+$target).slideToggle();
});


//função para reaproveitar listagem de usuários
function makeListUsers(res){
    var html = '';

    $('.box-user-list .card').html("");

    for(user in res){
        var position = parseInt(user) + 1;
        var nome = res[user].name.title + ' ' + capitalizeFirstLetter(res[user].name.first);
        var qtyInteractions = res[user].interactions.total;

        var shares = res[user].interactions.interactions.shares;
        var comments = res[user].interactions.interactions.comments;
        var favorites = res[user].interactions.interactions.favorites;

        html = '<div class="card__item" "><div class="card__content"><div class="position">'+position+'º</div><div class="name">'+nome+'</div><div class="qty-interactions">'+qtyInteractions+' interações</div><div class="details" data-id="'+user+'">+</div></div>  <div style="display: none;"  class="card__canvas" id="card__canvas'+user+'"><canvas  id="myChart'+user+'" width="200" height="200"></canvas></div> </div>';

        $('.box-user-list .card').append(html);

        var ctx = document.getElementById("myChart"+user);
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Compartilhamentos", "Commentários", "Favoritadas"],
                datasets: [{
                    label: '',
                    data: [shares,comments,favorites],
                    backgroundColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend:{
                    labels:{
                        fontColor: 'white'
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

    }
}


function listarTodos(){
    //listando usuários
    user.listUsers(function(res){
        makeListUsers(res);
    });


}

//função para adicionar itens de filtro
function setFilterItens(brandName){
    $('.box-user-list-header h1').text('Filtrando pela marca: ' + brandName);
    if(!$('.clean-filter').hasClass('included')){
        $('.box-user-list-header').append("<a href='' class='clean-filter included'>limpar filtro</a>");
    }

}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}