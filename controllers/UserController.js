class UserController{

    constructor(){
        var _self = this;

    }

    getUsers(fn){
        $.get('/data/users.json', (res) => {
            fn(res);
        });
    }

    listUsers(fn){
        var interactionController = new InteractionsController();

        this.getUsers(function(res){
            res.map((user) => {
                var interaction = interactionController.getInteractionsByUser(user.id);

                user.interactions = interaction;
            });

            var users = res.sort((a,b) => {
                return (a.interactions.total > b.interactions.total) ? -1 : (b.interactions.total > a.interactions.total) ? 1 : 0
            });
            fn(users);
        });
    }

    filterUsers(brand, ids, fn){
        var interactionController = new InteractionsController();
        this.getUsers(function(res){

            var filtered = res.filter(function(item){
                if(ids.indexOf(item.id) != -1){

                    var interaction = interactionController.getInteractionsByUserInBrand(brand, item.id);

                    item.interactions = interaction;

                    return item;
                }
            });

            var users = filtered.sort((a,b) => {
                return (a.interactions.total > b.interactions.total) ? -1 : (b.interactions.total > a.interactions.total) ? 1 : 0
            });

            fn(users);
        });

    }

}