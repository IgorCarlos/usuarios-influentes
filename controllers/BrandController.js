class brandController{
    constructor(){
        this.Brands = null;

    }

    getBrands(fn){
        $.get('/data/brands.json', (res) => {
            fn(res);
        });
    }

    listBrands(fn){
        var interactions = new InteractionsController();
        this.getBrands(function(res){
            res.map((brand) => {
                var allInteractions = interactions.getInteractionsByBrand(brand.id);

                brand.interactions = allInteractions.interactions;
                brand.qtyInteractions = allInteractions.total;
            });

            //ordena marcas em quantidade de interações
            var brands = res.sort((a,b) => {
                return (a.qtyInteractions > b.qtyInteractions) ? -1 : (b.qtyInteractions > a.qtyInteractions) ? 1 : 0
            });

            fn(brands);
        });
    }


    filterBrandUsers(brand, fn){
        var user = new UserController();

        var interaction = new InteractionsController();


        setTimeout(() => {
            var interactions = interaction.getInteractionsByBrand(brand);


            var inter = interactions.interactions.all;

            var users = [];
            for(interaction in inter){

                users.push(inter[interaction].user);
            }

            var objUsers = user.filterUsers(brand, users, function(res){
                fn(res);
            });
        },500);

    }
}