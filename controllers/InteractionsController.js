class InteractionsController{

    constructor(){
        this.interactions = null;
        this.quantityInteractions = null;
        this.getInteractions();
    }

    getInteractions(){
        $.get('/data/interactions.json', (res) => {
            this.interactions = res;
        });
    }


    getInteractionsByUserInBrand(brand, id){
        var shares = [];
        var favorites = [];
        var comments = [];

        var filtered = this.interactions.filter((item) => {

            if (item.user == id && item.brand == brand) {
                if(item.type == "SHARE"){
                    shares.push(item);
                }else if(item.type == "FAVORITE"){
                    favorites.push(item);
                }else if(item.type == "COMMENT"){
                    comments.push(item);
                }
                return item;
            }
        });

        var objInteractions = {
            interactions :{
                shares: shares.length,
                favorites: favorites.length,
                comments: comments.length,
                all: filtered
            },
            total: filtered.length
        }

        return objInteractions;
    }

    getInteractionsByUser(id) {

        var shares = [];
        var favorites = [];
        var comments = [];

        var filtered = this.interactions.filter((item) => {
            if (item.user == id) {
                if(item.type == "SHARE"){
                    shares.push(item);
                }else if(item.type == "FAVORITE"){
                    favorites.push(item);
                }else if(item.type == "COMMENT"){
                    comments.push(item);
                }
                return item;
            }
        });

        var objInteractions = {
            interactions :{
                shares: shares.length,
                favorites: favorites.length,
                comments: comments.length,
                all: filtered
            },
            total: filtered.length
        }

        return objInteractions;
    }

    getInteractionsByBrand(id) {
        var shares = [];
        var favorites = [];
        var comments = [];

        var filtered = this.interactions.filter((item) => {
            if(item.brand == id){
                if(item.type == "SHARE"){
                    shares.push(item);
                }else if(item.type == "FAVORITE"){
                    favorites.push(item);
                }else if(item.type == "COMMENT"){
                    comments.push(item);
                }
                return item;
            }
        });
        var objInteractions = {
            interactions :{
                shares: shares.length,
                favorites: favorites.length,
                comments: comments.length,
                all: filtered
            },
            total: filtered.length
        }

        return objInteractions;
    }
}